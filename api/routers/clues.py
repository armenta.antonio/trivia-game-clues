from fastapi import APIRouter, Response, status
from pydantic import BaseModel
import psycopg
from routers import categories

router = APIRouter()

@router.get("/api/clues/{clue_id}")
def get_clue(clue_id: int):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute("""
                SELECT 
                    clue.id, 
                    clue.answer, 
                    clue.question, 
                    clue.value,
                    clue.invalid_count, 
                    category.id, 
                    category.title,
                    category.canon, 
                    clue.canon
                FROM clues as clue
                INNER JOIN categories AS category ON (clue.category_id = category.id)
                WHERE clue.id = %s;
            """, [clue_id])
            # If you use JOIN, that means INNER JOIN

            clue = cur.fetchall()
            clue = clue[0]

            return {
                # "id": clue[0],
                "answer": clue[1],
                "question": clue[2],
                "value": clue[3],
                "invalid_count": clue[4],
                "category": {
                    "id": clue[5],
                    "title": clue[6],
                    "canon": clue[7],
                },
            }
