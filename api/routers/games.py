import random
from fastapi import APIRouter, Response, status
from pydantic import BaseModel
import psycopg

router = APIRouter()

class GameOut(BaseModel):
    id: int
    episode_id: int
    aired: str
    canon: bool
    total_amount_won: int

class Message(BaseModel):
    message: str

@router.get(
    "/api/games/{game_id}", 
    response_model=GameOut, 
    responses={404: {"model": Message}},
)
def get_game(game_id: int, response: Response):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute("""
            SELECT 
              games.id
            , games.episode_id
            , games.aired
            , games.canon
            , SUM(clues.value) AS total_amount_won
            FROM games
            LEFT OUTER JOIN clues ON(clues.game_id = games.id)
            WHERE games.id = %s
            GROUP BY 
              games.id;
            """, [game_id],
            )
            row = cur.fetchone()
            if row is None:
                response.status_code = status.HTTP_404_NOT_FOUND
                return {"message": "Category not found"}
            record = {}
            for i, column in enumerate(cur.description):
                record[column.name] = row[i]
            print(game_id)
            return record


@router.get("/api/custom-games")
def create_random_game():
#  random() in query